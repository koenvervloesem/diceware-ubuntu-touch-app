#include "rng.h"

RNG::RNG() {

}

unsigned int RNG::randomInt(unsigned int max) {
    /* Use this instead of JavaScipt's Math.random() because
       QRandomGenerator is a cryptographically safe random number generator. */
    return (unsigned int)QRandomGenerator::global()->bounded((quint32)max);
}
