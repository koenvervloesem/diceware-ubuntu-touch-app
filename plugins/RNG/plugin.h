#ifndef RNG_PLUGIN_H
#define RNG_PLUGIN_H

#include <QQmlExtensionPlugin>

class RNGPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
