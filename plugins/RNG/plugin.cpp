#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "rng.h"

void RNGPlugin::registerTypes(const char *uri) {
    //@uri RNG
    qmlRegisterSingletonType<RNG>(uri, 1, 0, "RNG", [](QQmlEngine*, QJSEngine*) -> QObject* { return new RNG; });
}
