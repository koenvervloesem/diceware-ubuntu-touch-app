#ifndef RNG_H
#define RNG_H

#include <QObject>
#include <QRandomGenerator>

class RNG : public QObject
{
    Q_OBJECT

public:
    RNG();
    ~RNG() = default;

    Q_INVOKABLE unsigned int randomInt(unsigned int max);
};

#endif
