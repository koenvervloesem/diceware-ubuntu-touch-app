import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: dialog
    title: i18n.tr("About Diceware")

	Rectangle {
		color: "transparent"
		width: parent.width
		height: width / 2
		Image {
			source: "assets/logo.svg"
			width: parent.width / 2
			height: width
			anchors.centerIn: parent
		}
	}

    Label {
		width: parent.width
		wrapMode: Text.WordWrap
        text: i18n.tr("Generates secure passphrases with") + " <a href='https://theworld.com/~reinhold/diceware.html'>Diceware</a>."
		onLinkActivated: Qt.openUrlExternally(link)
    }

    Label {
        text: "<a href='https://gitlab.com/koenvervloesem/diceware-ubuntu-touch-app'>" + i18n.tr("Source code") + "</a>"
		onLinkActivated: Qt.openUrlExternally(link)
    }

    Label {
        text: i18n.tr("Copyright © 2022 Koen Vervloesem")
    }

    Label {
        text: i18n.tr("Licensed under the MIT license")
    }

    Button {
        text: i18n.tr("Close")
        onClicked: PopupUtils.close(dialog)
    }
}
