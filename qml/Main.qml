import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.XmlListModel 2.0
import Qt.labs.settings 1.0
import RNG 1.0
import "diceware.js" as Diceware

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'diceware.koenvervloesem'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property var wordList: []
    property var passphrase: []
    property int nWords: 6
    property int entropy: Diceware.passphraseEntropy(nWords)
    property string language: "English"
    property int languageIndex: 0
    property string wordListName: "EFF long word list"
    property int wordListIndex: 0
    property bool spaces: true
    property bool capitalize: false

    Settings {
        property alias nWords: root.nWords
        property alias language: root.language
        property alias languageIndex: root.languageIndex
        property alias wordListName: root.wordListName
        property alias wordListIndex: root.wordListIndex
        property alias spaces: root.spaces
        property alias capitalize: root.capitalize
    }

    XmlListModel {
        id: languageModel
        source: "../assets/wordlists/wordlists.xml"
        query: "/diceware/language"

        XmlRole { name: "language"; query: "@name/string()" }
    }

	XmlListModel {
		id: wordListModel
		source: "../assets/wordlists/wordlists.xml"
		query: "/diceware/language[@name='" + root.language + "']/wordlist"

		XmlRole { name: "wordList"; query: "@name/string()" }
		XmlRole { name: "path"; query: "@path/string()" }

		onStatusChanged: {
			if (status == XmlListModel.Ready) {
				Diceware.changeWordList(wordListModel, root.wordListIndex);
			}
		}
	}

    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("MainPage.qml"));
    }

    PageStack {
        id: pageStack
    }

}
