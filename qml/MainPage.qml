import QtQuick 2.7
import Ubuntu.Components 1.3 as UC
import Ubuntu.Components.ListItems 1.3 as ListItems
import Ubuntu.Components.Popups 1.3
import QtQuick.Controls 2.2
import "diceware.js" as Diceware

Page {

    visible: false
    anchors.fill: parent

    Component {
        id: aboutDialog
        AboutDialog {}
    }

    header: UC.PageHeader {
        title: i18n.tr('Diceware')

        UC.ActionBar {
			anchors {
				top: parent.top
				right: parent.right
				topMargin: units.gu(1)
				rightMargin: units.gu(1)
			}
			numberOfSlots: 1
			actions: [
				UC.Action {
					iconName: "info"
					text: i18n.tr("About")
					onTriggered: PopupUtils.open(aboutDialog)
				}
			]
        }
    }

    Column {
        id: column
        anchors {
            left: parent.left
            right: parent.right
        }
        UC.ListItem {
            height: capitalizeLayout.height + divider.height
            Label {
                text: i18n.tr('Words')
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    left: parent.left
                    topMargin: units.gu(1)
                    bottomMargin: units.gu(1)
                    leftMargin: units.gu(2)
                }
                verticalAlignment: Label.AlignVCenter
            }
            SpinBox {
                value: root.nWords
                from: 1
                editable: true
                onValueModified: {
                    root.nWords = value;
                    Diceware.generatePassphrase();
                }
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                    topMargin: units.gu(1)
                    bottomMargin: units.gu(1)
                    rightMargin: units.gu(2)
                }
            }
        }
        UC.ListItem {
            height: languageLayout.height + divider.height
            anchors {
                left: parent.left
                right: parent.right
            }
            onClicked: pageStack.push(Qt.resolvedUrl("LanguagePage.qml"));
            UC.ListItemLayout {
                id: languageLayout
                height: capitalizeLayout.height
                title.text: i18n.tr("Language")
                Label {
                    text: root.language
                }
                UC.ProgressionSlot {}
            }
        }
        UC.ListItem {
            height: wordListLayout.height + divider.height
            anchors {
                left: parent.left
                right: parent.right
            }
            onClicked: pageStack.push(Qt.resolvedUrl("WordListPage.qml"));
            UC.ListItemLayout {
                id: wordListLayout
                height: capitalizeLayout.height
                title.text: i18n.tr("Word list")
                Label {
                    id: wordListLabel
                    text: root.wordListName
                }
                UC.ProgressionSlot {}
            }
        }
        UC.ListItem {
            height: spacesLayout.height + divider.height
            anchors {
                left: parent.left
                right: parent.right
            }
            UC.ListItemLayout {
                id: spacesLayout
                title.text: i18n.tr("Spaces")
                UC.Switch {
                    id: spacesSwitch
                    checked: root.spaces
                    onClicked: {
                        root.spaces = checked;
                    }
                }
            }
        }
        UC.ListItem {
            height: capitalizeLayout.height + divider.height
            anchors {
                left: parent.left
                right: parent.right
            }
            UC.ListItemLayout {
                id: capitalizeLayout
                title.text: i18n.tr("Capitalize")
                UC.Switch {
                    id: capitalizeSwitch
                    checked: root.capitalize
                    onClicked: {
                        root.capitalize = checked;
                    }
                }
            }
        }

    }

    Label {
        id: passphraseLabel
        text: Diceware.showPassphrase(root.spaces, root.capitalize)
        anchors {
            top: column.bottom
            bottom: generateLabel.top
            left: parent.left
            right: parent.right
            topMargin: units.gu(2)
            bottomMargin: units.gu(2)
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
        verticalAlignment: Label.AlignVCenter
        horizontalAlignment: Label.AlignHCenter
        width: parent.width
        wrapMode: Text.Wrap
        font.pixelSize: FontUtils.sizeToPixels("x-large")
    }

    Label {
        id: minimumLengthLabel
        text: i18n.tr('Warning: passphrase is shorter than %1 characters').arg(Diceware.minimumPassPhraseLength)
        anchors {
            bottom: entropyLabel.top
            bottomMargin: units.gu(2)
        }
        horizontalAlignment: Label.AlignHCenter
        width: parent.width
        visible: passphraseLabel.text.length < Diceware.minimumPassPhraseLength
        color: "red"
    }

    Label {
        id: entropyLabel
        text: i18n.tr('Entropy')
        anchors {
            bottom: generateLabel.top
            left: parent.left
            bottomMargin: units.gu(2)
            leftMargin: units.gu(2)
        }
    }
    Label {
        text: i18n.tr("%1 bits").arg(root.entropy)
        anchors {
            bottom: generateLabel.top
            right: parent.right
            bottomMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
    }

    Button {
        id: generateLabel
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            bottomMargin: units.gu(2)
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
        text: i18n.tr("Generate")
        onClicked: Diceware.generatePassphrase()
    }
}
