import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItems
import QtQuick.Controls 2.2
import "diceware.js" as Diceware

Page {

	visible: false
	anchors.fill: parent

	header: PageHeader {
		title: i18n.tr('Language')
	}

	Column {
		anchors {
			top: parent.top
			bottom: parent.bottom
			left: parent.left
			right: parent.right
			topMargin: units.gu(2)
		}
		ListItems.ItemSelector {
			id: languageSelector
			expanded: true
			model: languageModel
			selectedIndex: root.languageIndex
			onDelegateClicked: {
				Diceware.changeLanguage(model, index);
			}
		}
	}
}
