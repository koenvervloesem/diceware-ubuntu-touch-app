const sizeWordList = 7776;
const regexpWordList = /^[0-9]+\s+([^\s]+)$/;
const wordListPath = "../assets/wordlists/";
const minimumPassPhraseLength = 20;

// Compute entropy in bits of a Diceware passphrase with a given number of words.
// Returns a string, e.g. "77 bits".
function passphraseEntropy(numberOfWords) {
	return Math.floor(numberOfWords * Math.log(sizeWordList) / Math.log(2));
}

// Generate new passphrase words
function generatePassphrase() {
	let dicewarePassphrase = [];
	for(let i=0; i<root.nWords; i++) {
		dicewarePassphrase.push(root.wordList[RNG.randomInt(root.wordList.length)]);
	}
	root.passphrase = dicewarePassphrase;
    showPassphrase(root.spaces, root.capitalize);
}

// Capitalize the first letter of a string
function capitalizeFirstLetter(str) {
    return str[0].toUpperCase() + str.slice(1);
}

// Show passphrase
function showPassphrase(spaces, capitalize) {
    let passphrase = [];
    if(capitalize) {
        passphrase = root.passphrase.map(word => capitalizeFirstLetter(word));
    } else {
        passphrase = root.passphrase;
    }

    return passphrase.join(spaces ? " " : "")
}

// Change language
function changeLanguage(model, index) {
    root.languageIndex = index;
    root.language = model.get(index).language;
	root.wordListIndex = 0;
    wordListModel.reload();
}

// Change word list
function changeWordList(model, index) {
	root.wordListIndex = index;
	root.wordListName = model.get(index).wordList;
    console.log(root.wordListName);
	loadWordList(wordListPath + model.get(index).path);
}

// Load a Diceware word list into an array.
function loadWordList(wordListFile) {
	const xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function() {
		if(xhr.readyState === XMLHttpRequest.DONE) {
			let wordList = [];

			// Split response on newlines and remove empty lines
			const response = xhr.responseText.toString().split(/\r?\n/).filter(line => line);

			// Fill array with each word
			response.forEach(line => {
                let match = regexpWordList.exec(line);
                if(match) {
                    // Only add lines that match the Diceware format.
                    // This ignores comments, PGP signatures, and so on.
				    wordList.push(match[1]);
                }
			});

			// Sanity check
			if(wordList.length != sizeWordList) {
				console.log("ERROR: Word list should have size " + sizeWordList + ", has size " + wordList.length);
			}

			root.wordList = wordList;
			generatePassphrase();
		}
	}
	xhr.open("GET", wordListFile);
	xhr.send();
}
