[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/diceware.koenvervloesem)

# Diceware Ubuntu Touch app

An Ubuntu Touch app that generates secure passphrases with [Diceware](https://theworld.com/~reinhold/diceware.html).

## What is Diceware?

Arnold G. Reinhold describes his invention [Diceware](https://theworld.com/~reinhold/diceware.html) as:

"Diceware™ is a method for picking passphrases that uses ordinary dice to select words at random from a special list called the Diceware Word List. Each word in the list is preceded by a five digit number. All the digits are between one and six, allowing you to use the outcomes of five dice rolls to select a word from the list."

With a couple of words chosen this way, you have a passphrase that is rather easy to remember but quite secure.

## What does this app do?

Instead of dice, this app uses Qt's [QRandomGenerator](https://doc.qt.io/qt-5/qrandomgenerator.html) class to generate cryptographically secure random numbers to choose words from a Diceware word list and generate a passphrase.

The app supports a couple of Diceware word lists in multiple languages. Just choose your language, word list, number of words and a few other parameters and start generating secure passphrases.

![Screenshot](screenshot.png)

Make sure to read the [Diceware passphrase FAQ](https://theworld.com/%7Ereinhold/dicewarefaq.html) before using this app, especially the question "How long should my passphrase be?".

## Included word lists

The app includes the following Diceware word lists:

### English

* [EFF long](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt) ([Info](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases))
* [Beale](https://theworld.com/%7Ereinhold/beale.wordlist.asc)
* [SecureDrop](https://raw.githubusercontent.com/heartsucker/diceware/develop/wordlists/en_US/wordlist-numbered.txt) ([Info](https://github.com/heartsucker/diceware))

### Dutch

* [Remko Tronçon's list with composite words](https://el-tramo.be/diceware/diceware-wordlist-composites-nl.txt) ([Info](https://el-tramo.be/blog/diceware-nl/))
* [Remko Tronçon's list with no composite words](https://el-tramo.be/diceware/diceware-wordlist-nl.txt) ([Info](https://el-tramo.be/blog/diceware-nl/))

### French

* [Arthur Pons](https://raw.githubusercontent.com/ArthurPons/diceware-fr-alt/master/diceware-fr-alt.txt) ([Info](https://github.com/ArthurPons/diceware-fr-alt))

## Contributing

Do you want to contribute to this app? Read how on our [CONTRIBUTING](CONTRIBUTING.md) page.

## License

Copyright (C) 2022 Koen Vervloesem

Licensed under the [MIT license](LICENSE).

The app's icon, licensed CC BY-SA 4.0, is based on the [Dice shield icon](https://game-icons.net/1x1/delapouite/dice-shield.html) by [Delapouite](https://delapouite.com/), [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), and [Ubuntu Icon Resource Kit](https://gitlab.com/cibersheep/ubuntu-icon-resource-kit)'s Ubuntu shape, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
