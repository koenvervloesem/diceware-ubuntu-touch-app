# Contributing to the Diceware Ubuntu Touch app

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

The procedure is:

1. Fork the project
2. Create your feature branch (`git checkout -b feature/AmazingFeature`)
3. Commit your changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the branch (`git push origin feature/AmazingFeature`)
5. Open a merge request

The easiest way to build this app is using Clickable by running the command:

```
clickable desktop
```

This builds the app for and runs it on your computer. If you want to build it for an ARM64 Ubuntu Touch phone, run Clickable like this:

```
clickable --arch arm64
```

Even if you're not a coder, you can contribute:

## Add a new Diceware word list

The word lists are stored in [assets/wordlists](assets/wordlists).

Add a new word list file to the right language directory, named according to the language's [ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes). If you want to add a new language, just create a directory for it first.

Then add a new element for the word list to the file [wordlists.xml](assets/wordlists/wordlists.xml). Look at the elements of other word lists for examples. Only the `name` and `path` attributes of a word list are compulsory. The other ones are highly recommended.

Note: Always add a new word list for a language after the other word lists of this language in `wordlists.xml`, and always add a new language after the other languages.

And last but not least, add an entry for the word list file to [assets.qrc](assets/assets.qrc).

Test the app with this new word list to make sure that it works. Have a close look at the debug output of `clickable desktop` for possible error messages while loading or using the word list.

## Translate the app to your own mother language

The language files are stored in the [po](po) directory. Translate the app to your own language by copying the [diceware.koenvervloesem.pot](po/diceware.koenvervloesem.pot) file and naming it after your language code with extension `po`. For each `msgid` string, put your translation in the corresponding `msgstr`. Have a look at the existing language files for help.

Test your language file by setting the language of Ubuntu Touch to the same language and starting your app there with `clickable`.
